# Final Project 2

# Kelompok 26

# Anggota Kelompok
- <h2>Rahmat Januardi</h2>
- <h2>Shofari Bagus</h2><br>

# Tema Project
<h2>Sistem Pengajuan Donor Darah</h2><br>

# ERD
<p align="center">
<img src="https://i.ibb.co/BLpMBjQ/photo-2021-12-11-16-18-43.jpg" width="400">
</p><br>

# Link Video
<h2>Link Video Demo Aplikasi :
    <a href="https://youtu.be/KNzs7gPucwA">https://youtu.be/KNzs7gPucwA</a>
</h2>
<br><br>

# Link Screenshot
<h2>Screenshot Aplikasi :
    <a href="https://drive.google.com/file/d/1c2bjzpUQHw9bXBZ7OK7NL9C1G9NhWAsB/view?usp=sharing">https://drive.google.com/file/d/1c2bjzpUQHw9bXBZ7OK7NL9C1G9NhWAsB/view?usp=sharing</a>
</h2>

# Link Web Service Laravel
<h2>Web Service Aplikasi :
    <a href="https://documenter.getpostman.com/view/8765674/UVR7L8e3">https://documenter.getpostman.com/view/8765674/UVR7L8e3</a>
</h2>