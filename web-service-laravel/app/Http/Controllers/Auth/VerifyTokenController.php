<?php

namespace App\Http\Controllers\auth;

use App\User;
use App\OtpCode;
use App\Profile;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VerifyTokenController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    public function __invoke(Request $request)
    {
        $user = Auth()->user();
        $profile = Profile::where('user_id',$user->id)->first();
        $role = Role::where('id',Auth()->user()->role_id)->first();
        return response()->json([
            'success' => true,
            'message' => 'User berhasil diverifikasi',
            'user' => $user,
            'profile' => $profile,
            'role'  => $role
        ], 200);
    }
}
