<?php

namespace App\Http\Controllers;

use App\Darah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DarahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index']);
    }

    public function index()
    {
        $darahs = Darah::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar darah berhasil ditampilkan',
            'darah'    => $darahs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'golongan' => 'required',
            'stock' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $darah = Darah::create([
            'golongan' =>  $request->golongan,
            'stock' =>  $request->stock,
        ]);

        if ($darah) {
            return response()->json([
                'success'   => true,
                'message'   => 'Data Darah berhasil dibuat',
                'data'      =>  $darah
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Darah gagal dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $darah = Darah::find($id);

        if ($darah) {
            return response()->json([
                'success' => true,
                'message' => 'Data darah berhasil ditampilkan',
                'data'    => $darah
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            //'golongan' => 'required',
            'stock' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $darah = Darah::find($id);

        if ($darah) {
            $darah->update([
                'golongan' =>  $request->golongan,
                'stock' =>  $request->stock,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data berhasil diupdate',
                'data' =>    $darah
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $darah = Darah::find($id);

        if ($darah) {

            $darah->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data darah berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }
}
