<?php

namespace App\Http\Controllers;

use App\Permintaan;
use App\PermintaanDarah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PermintaanController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api');
    }
    /**->only(['store' , 'update' , 'delete']);
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth()->user()->role_id == 'e0cf7c5a-7d08-4e11-bd6d-06fc8a355351'){
            $permintaans = Permintaan::join('users','users.id','=','permintaans.user_id')->orderBy('created_at','Desc')->select('permintaans.*','users.name')->get();
        }
        else{
            $permintaans = Permintaan::whereUserId(Auth()->user()->id)->orderBy('created_at','DESC')->get();
        }
        //$permintaans = Permintaan::where('user_id',Auth()->user()->id)->orderBy('created_at','DESC')->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar Permintaan berhasil ditampilkan',
            'data'    => $permintaans
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        //dd($request->darah);
        $validator = Validator::make($allRequest , [
            'status' => 'required',
            'darah' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $permintaan = Permintaan::create([
            'status' =>  $request->status,
        ]);

        //dd($request->darah);
        //$darah
        if($permintaan){
            //dd($request->darah);

            foreach($request->darah as $index => $value){
                $data = [
                    'permintaan_id' => $permintaan->id,
                    'darah_id'      => $value['id'],
                    'jumlah'        => $value['jumlah']
                ];
                PermintaanDarah::create($data);
            }
            return response()->json([
                'success'   => true,
                'message'   => 'Data Permintaan berhasil dibuat',
                'data'      =>  $permintaan
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Permintaan gagal dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permintaan = Permintaan::find($id);
        $user = $permintaan->user->profile;
        $darah = $permintaan->permintaan_darah;
        foreach($darah as $item){
            $item->darah;
        }
        //dd($darah);
        if($permintaan)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data Permintaan berhasil ditampilkan',
                'pengajuan'     => $permintaan,
                'user'          => $user,
                'darah'         => $darah
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $permintaan = Permintaan::find($id);

        if($permintaan)
        {
            $user = auth()->user();

            if($user->role_id != 'e0cf7c5a-7d08-4e11-bd6d-06fc8a355351')
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data permintaan bukan milik user admin',
                ] , 403);

            }

            $permintaan->update([
                'status' => $request->status,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data dengan status berhasil diupdate',
                'data' =>    $permintaan
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permintaan = Permintaan::find($id);

        if ($permintaan) {
            $user = auth()->user();

            if ($permintaan->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data permintaan bukan milik user admin',
                ], 403);
            }

            $permintaan->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data permintaan berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }
}
