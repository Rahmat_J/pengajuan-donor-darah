<?php

namespace App\Http\Controllers;

use App\PermintaanDarah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PermintaanDarahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $darah_id = $request->darah_id;

        $permintaan_id = $request->permintaan_id;

        $permintaandarahs = PermintaanDarah::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar Permintaan  berhasil ditampilkan',
            'data'    => $permintaandarahs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'jumlah' => 'required',
            'darah_id' => 'required',
            'permintaan_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $permintaandarah = PermintaanDarah::create([
            'jumlah' =>  $request->jumlah,
            'darah_id' => $request->darah_id,
            'permintaan_id' => $request->permintaan_id,
        ]);

        // event(new CommentStoredEvent($comment));

        if ($permintaandarah) {
            return response()->json([
                'success'   => true,
                'message'   => 'Data Permintaan Darah berhasil dibuat',
                'data'      =>  $permintaandarah
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Permintaan Darah gagal dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permintaandarah = PermintaanDarah::find($id);

        if ($permintaandarah) {
            return response()->json([
                'success' => true,
                'message' => 'Data permintaan darah berhasil ditampilkan',
                'data'    => $permintaandarah
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'jumlah' => 'required',
            'darah_id' => 'required',
            'permintaan_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $permintaandarah = PermintaanDarah::find($id);

        if ($permintaandarah) {

            // $user = auth()->user();

            // if ($permintaandarah->user_id != $user->id) {
            //     return response()->json([
            //         'success' => false,
            //         'message' => 'Data Permintaan Darah bukan milik user login',
            //     ], 403);
            // }

            $permintaandarah->update([
                'jumlah' =>  $request->jumlah,
                'darah_id' => $request->darah_id,
                'permintaan_id' => $request->permintaan_id,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Permintaan Darah berhasil diupdate',
                'data' =>    $permintaandarah
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permintaandarah = PermintaanDarah::findOrfail($id);

        if($permintaandarah) {

            $user = auth()->user();

            if($permintaandarah->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ] , 403);
            }

            //delete comment
            $permintaandarah->delete();

            return response()->json([
                'success' => true,
                'message' => 'Permintaan Darah Deleted',
            ], 200);

        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'Permintaan Darah Not Found',
        ], 404);
    }
}
