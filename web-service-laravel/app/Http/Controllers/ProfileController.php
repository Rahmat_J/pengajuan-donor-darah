<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        return $this->middleware('auth:api');
    }

    public function index()
    {
        $profiles = Profile::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data'    => $profiles
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [
            'nama' => 'required',
            'alamat' => 'required',
            'instansi' => 'required',
            'alamat_instansi' => 'required',
            'phone' => 'required',
            'phone_instansi' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $profile = Profile::create([
            'nama' =>  $request->nama,
            'alamat' => $request->alamat,
            'instansi' =>  $request->instansi,
            'alamat_instansi' => $request->alamat_instansi,
            'phone' =>  $request->phone,
            'phone_instansi' => $request->phone_instansi,
        ]);

        if($profile){
            return response()->json([
                'success'   => true,
                'message'   => 'Data Profil berhasil dibuat',
                'data'      =>  $profile
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Profile gagal dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::find($id);

        if($profile)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data profile berhasil ditampilkan',
                'data'    => $profile
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'nama' => 'required',
            'alamat' => 'required',
            'instansi' => 'required',
            'alamat_instansi' => 'required',
            'phone' => 'required',
            'phone_instansi' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $profile = Profile::find($id);

        if($profile)
        {
            $user = auth()->user();

            if($profile->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data profile bukan milik user login',
                ] , 403);

            }

            $profile->update([
                'nama' =>  $request->nama,
                'alamat' => $request->alamat,
                'instansi' =>  $request->instansi,
                'alamat_instansi' => $request->alamat_instansi,
                'phone' =>  $request->phone,
                'phone_instansi' => $request->phone_instansi,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data berhasil diupdate',
                'data' =>    $profile
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::find($id);

        if ($profile) {
            $user = auth()->user();

            if ($profile->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data profile bukan milik user login',
                ], 403);
            }

            $profile->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data profile berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }
}
