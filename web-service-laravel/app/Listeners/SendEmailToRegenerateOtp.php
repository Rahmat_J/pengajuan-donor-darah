<?php

namespace App\Listeners;

use App\Events\RegenerateOtp;
use App\Mail\RegenerateOtpMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailToRegenerateOtp implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateOtp  $event
     * @return void
     */
    public function handle(RegenerateOtp $event)
    {
        Mail::to($event->otp_code->user->email)->send(new RegenerateOtpMail($event->otp_code));
    }
}
