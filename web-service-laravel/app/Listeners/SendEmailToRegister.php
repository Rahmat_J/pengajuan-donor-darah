<?php

namespace App\Listeners;

use App\Events\RegisterOtp;
use App\Mail\RegisterMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailToRegister implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterOtp  $event
     * @return void
     */
    public function handle(RegisterOtp $event)
    {
        Mail::to($event->otp_code->user->email)->send(new RegisterMail($event->otp_code));
    }
}
