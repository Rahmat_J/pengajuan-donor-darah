<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PermintaanDarah extends Model
{
    protected $fillable = ['jumlah', 'darah_id', 'permintaan_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }

            // $model->user_id = auth()->user()->id;
        });
    }

    public function darah(){
        return $this->belongsTo('App\Darah');
    }

    public function permintaan(){
        return $this->belongsTo('App\Permintaan');
    }
}
