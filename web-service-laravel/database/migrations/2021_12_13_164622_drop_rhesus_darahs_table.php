<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropRhesusDarahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('darahs', function (Blueprint $table) {
            // The "users" table exists and has an "middle_name" column...
            if (Schema::hasColumn('darahs', 'rhesus')) {
                $table->dropColumn('rhesus');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
