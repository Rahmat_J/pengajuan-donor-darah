<?php

use Illuminate\Database\Seeder;
use App\Darah;
use App\Role;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $darah = [
            ['golongan' => 'A+', 'stock' => '100'],
            ['golongan' => 'A-', 'stock' => '100'],
            ['golongan' => 'B+', 'stock' => '100'],
            ['golongan' => 'B-', 'stock' => '100'],
            ['golongan' => 'AB+', 'stock' => '100'],
            ['golongan' => 'AB-', 'stock' => '100'],
            ['golongan' => 'O+', 'stock' => '100'],
            ['golongan' => 'O-', 'stock' => '100'],

        ];

        foreach($darah as $item){
            Darah::create([
                'golongan' => $item['golongan'],
                'stock' => $item['stock']
            ]);
        };

        $role = [
            ['id' => 'e0cf7c5a-7d08-4e11-bd6d-06fc8a355351', 'deskripsi' => 'administrator'],
            ['id' => '73083da3-e375-4d07-978d-39dd8a53923d', 'deskripsi' => 'member']
        ];

        foreach($role as $item){
            Role::insert($item);
        }

        $admin = [
            'id' => 'b5a4660a-5cf4-11ec-bf63-0242ac130002',
            'name' => 'administrator',
            'email' => 'admin@admin.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'email_verified_at' => Carbon\Carbon::now(),
            'role_id' => 'e0cf7c5a-7d08-4e11-bd6d-06fc8a355351',
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
        ];

        User::insert($admin);
    }
}
