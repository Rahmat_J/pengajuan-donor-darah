<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('role', 'RoleController');
Route::apiResource('darah', 'DarahController');
Route::apiResource('permintaan', 'PermintaanController');
Route::apiResource('permintaan-darah', 'PermintaanDarahController');
Route::apiResource('profile', 'ProfileController');

Route::group([
    'prefix' => 'auth' ,
    'namespace' => 'Auth' ,
] , function(){
    Route::post('register' , 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate_otp_code');
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login', 'LoginController')->name('auth.login');
    Route::post('me','VerifyTokenController')->name('auth.check');
    Route::post('logout', 'LogoutController')->name('auth.logout');
});
