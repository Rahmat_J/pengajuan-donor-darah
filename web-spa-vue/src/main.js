import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import axios from './plugins/axios'
import store from './store'
import AsyncComputed from 'vue-async-computed'


Vue.config.productionTip = false
Vue.use(AsyncComputed)

new Vue({
  vuetify,
  router,
  store,
  axios,
  render: h => h(App)
}).$mount('#app')
