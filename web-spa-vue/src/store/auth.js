import axios from 'axios';
export default ({
    namespaced : true,
    state : {
        token :  '',
        user : {},
        profile : {},
    },
    mutations : {
        setToken : (state, payload) => {
            state.token = payload
        },
        setUser : (state, payload) => {
            state.user = payload 
        },
        setProfile : (state, payload) => {
            state.profile = payload
        },

    },
    actions : {
        setToken :  ({commit, dispatch}, payload) => {
            commit('setToken', payload)
            dispatch('checkToken', payload)
        },

        checkToken : ({commit}, payload) => {

            let config = {
                method : 'post',
                url : 'http://localhost:8000/api/auth/me',
                headers : {
                    'Authorization' : 'Bearer ' + payload,
                }
            }

            axios(config)
            .then( (response) => {
                console.log(response.data)
                commit('setUser', response.data.user)
                commit('setProfile',response.data.profile)

            })
            .catch( () => {
                commit('setUser', {})
                commit('setToken', '')
                commit('setProfile',{})
            })
        },
        setUser : ({commit}, payload) => {
            commit('setUser', payload)
        }
    },
    getters :  {
        user : state => state.user,
        token : state => state.token,
        guest : state => Object.keys(state.user).length === 0,
        profile : state => state.profile,
    }
})