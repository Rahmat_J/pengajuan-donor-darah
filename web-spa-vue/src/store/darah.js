import Vue from "vue";
import axios from "axios";

export default({
    namespaced : true,
    state : {
        darah : []
    },
    mutations : {
        set : (state, payload) => {
            Vue.set(state, 'darah', payload);
        },
    },
    actions : {
        fetchDarah : ({commit}) => {
            let config = {
                method : 'get',
                url : 'http://localhost:8000/api/darah'
            }

            axios(config)
            .then( (response) => {
                let {darah} = response.data
                let tampung = darah
                commit('set',tampung)
            } )
            .catch( ()  => {
                commit('set', [])
            })
        },
    },
    getters : {
        darah : state => state.darah,
    }
})