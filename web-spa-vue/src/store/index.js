import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import alert from './alert'
import setting from './setting'
import dialog from './dialog'
import darah from './darah'
import auth from './auth'

const vuexPersist = new VuexPersist({
    key: 'pkgdigischool',
    storage: localStorage
});

Vue.use(Vuex)

export default new Vuex.Store({
    plugins : [vuexPersist.plugin],
    modules : {
        alert,
        setting,
        dialog,
        darah,
        auth
    }
})