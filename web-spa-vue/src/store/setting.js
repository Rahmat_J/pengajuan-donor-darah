export default({
    namespaced : true,
    state : {
        apiDomain : 'http://localhost:8000'
    },
    mutations : {

    },
    actions : {

    },
    getters : {
        apiDomain : state => state.apiDomain
    }
})